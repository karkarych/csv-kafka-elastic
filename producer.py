# from logging import PlaceHolder
import time
from venv import create
import requests
from kafka import KafkaProducer
from kafka.errors import KafkaError
from kafka.admin import KafkaAdminClient, NewTopic
import json
import os
import csv_to_json

config = json.loads(open('config.json', 'r').read())


admin = KafkaAdminClient(
    bootstrap_servers=config['kafka']['hostname'],
    client_id='admin'
)

# I didn't manage to fix error in this function, so let it have rest here
#  
# def deleteTopic(topic_name):
#     try:
#         admin.delete_topics(topics=[topic_name])
#         print(f"Topic `{topic_name}` deleted successfully")
#     except UnknownTopicOrPartitionError as e:
#         print(f"Topic `{topic_name}` didn't ever exist")
#     except Exception as e:
#         print(e)


def createTopic(topic_name):
    print('Actual topics:', admin.list_topics())
    if topic_name not in admin.list_topics():
        topic = NewTopic(name=topic_name,
                         num_partitions=1,
                         replication_factor=1)
        admin.create_topics(new_topics=[topic], validate_only=False)
        return True
    else:
        print(f"Topic `{topic_name}` already exists")
        # ans = input(
        #     f"Topic `{topic_name}` already exists. Type `Y` to delete: ") or None
        # if ans == 'Y':
        #     deleteTopic(topic_name)
        #     createTopic(topic_name)


def dispatchEvent(topic_name, data):
    producer = KafkaProducer(bootstrap_servers=[config['kafka']['hostname']])
    future = producer.send(topic_name, data)
    try:
        future.get(timeout=10)
        print('Sent')
        return True
    except KafkaError as e:
        print('Error')
        return False


def getForecast():
    city = 'Chisinau'
    api = 'dc03d8a5270d6b4198543bf09078a44f'
    rawData = requests.get(
        f'https://api.openweathermap.org/data/2.5/weather?q={city}&appid={api}')
    return rawData


def getParsedCSV(src):
    csv_to_json.convert(src)
    parsed = json.loads(open(f"{src}.json", 'r').read())
    return parsed


createTopic(config['kafka']['topic'])

rows_to_send = getParsedCSV(config['source']['csv'])

for i in range(len(rows_to_send)):
    print(f"ROW {i+1}\t\tID {rows_to_send[i].get('ID')}\t\t STATUS ", end="")
    dispatchEvent(config['kafka']['topic'], bytes(
        json.dumps(rows_to_send[i]), 'utf-8'))
    # time.sleep(5)
